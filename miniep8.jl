function troca(v, i, j)
	
	aux = v[i]
	v[i] = v[j]
	v[j] = aux

end

function insercao(v)

	tam = length(v)
	for i in 2:tam

		j = i

		while j > 1

		
			if compareByValueAndSuit(v[j], v[j - 1])]
 
				troca(v, j, j - 1)
			else

			break

			end

		j = j - 1

		end
	end

	return v

end

function compareByValue(x, y)

        xsemnaipe = replace(x, r"[♠♥♦♣]" => "")
        ysemnaipe = replace(y, r"[♠♥♦♣]" => "")
        Jx = replace(xsemnaipe, "J" => "11")
        Jy = replace(ysemnaipe, "J" => "11")
        Qx = replace(Jx, "Q" => "12")
        Qy = replace(Jy, "Q" => "12")
        Kx = replace(Qx, "K" => "13")
        Ky = replace(Qy, "K" => "13")
        Ax = replace(Kx, "A" => "14")
        Ay = replace(Ky, "A" => "14")
        
        Axnumber = parse(Int, Ax)
        Aynumber = parse(Int, Ay)
        
        if Axnumber < Aynumber
            
            return true
        else
            
            return false
            
        end
        
end

function compareByValueAndSuit(x, y)

    xseparado = split(x, "")
    yseparado = split(y, "")
    xfinal = ""
    yfinal = ""
    
        if length(xseparado) == 3
    
            xfinal = xseparado[1] * xseparado[2] * " " * xseparado[3]
            xfim = split(xfinal, " ")
              
        else 
        
            xfim= xseparado
            
        end
        
        if length(yseparado) == 3
    
            yfinal = yseparado[1] * yseparado[2] * " " * yseparado[3]
            yfim = split(yfinal, " ")
        else
        
            yfim = yseparado
    
        end
        
        Jx = replace(xfim, "J" => "11")
        Jy = replace(yfim, "J" => "11")
        Qx = replace(Jx, "Q" => "12")
        Qy = replace(Jy, "Q" => "12")
        Kx = replace(Qx, "K" => "13")
        Ky = replace(Qy, "K" => "13")
        Ax = replace(Kx, "A" => "14")
        Ay = replace(Ky, "A" => "14")
        
        ourox = replace(Ax, "♦" => "1" ) 
        ouroy = replace(Ay, "♦" => "1" ) 
        espadax = replace(ourox, "♠" => "14" ) 
        espaday = replace(ouroy, "♠" => "14" ) 
        copasx = replace(espadax, "♥" => "27" ) 
        copasy = replace(espaday, "♥" => "27" ) 
        pausx = replace(copasx, "♣" => "40" ) 
        pausy = replace(copasy, "♣" => "40" ) 
        
        xumend = parse(Int, pausx[1])
        xdoisend = parse(Int, pausx[2])
        
        somax = xumend + xdoisend
        
        yumend = parse(Int, pausy[1])
        ydoisend = parse(Int, pausy[2])
        
        somay = yumend + ydoisend
        
    if somax < somay
        
        return true
        
    else
    
        return false

    end
    
end

using Test

    function testaCompareByValue()
        
        @test compareByValue("2♠", "A♠")
        
        @test compareByValue("8♠", "10♠")
        
        @test compareByValue("K♥", "10♥")
        
        println("Final dos testes")
    
    end
    
    function testaCompareByValueAndSuit() 
    
        @test compareByValue("2♠", "A♠")
        
        @test compareByValue("10♠", "10♥")
        
        @test compareByValue("A♠", "2♥")
        
        @test compareByValueAndSuit("K♥", "10♥")
        
        println("Final dos testes")
    
    end
    
testaCompareByValue()

testaCompareByValueAndSuit()
